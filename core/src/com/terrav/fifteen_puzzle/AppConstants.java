package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.math.RandomXS128;

import java.util.Date;

public class AppConstants {

    public static final RandomXS128 RANDOM_XS128 = new RandomXS128(new Date().getTime());

    public static final String APP_NAME = "15-Puzzle";
    public static final String PUBLIC_RSA_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArwiwKnMfh7xY5nYGJwsxrQ7y6fHnQMYb8oQAj83z1xIrQBysK825cZcesAmQgueWcla9OWrmhOd4q8M1HpYEOh1l85zTnecv7GbJKN/MWMoYzsNX+nnIU42h/PV2HaArs0OhD46PyOPxluS4GYC0Bxi+k1cFUKLBzRt+B8v93vXPwARa6by1jHBLgx4AoBufc5iw+qJZi0yKip5vhKx5WYuLVJS64VHFK9KP6+oj5HObK9JbbC1SiQsbzHtpZN1qvuCI/cRqkMtxnbbGcPH68S4kADFwIdYDitb/t3GmrXXYl4uDIP/3Ul11p94pGs3mZsgEwRPsAVDFh1V05zkejwIDAQAB";
    public static final String SKU_THANK_YOU = "thank.you";


    public static final float APP_WIDTH = 1600;
    public static final float APP_HEIGHT = 900;
    public static final float PADDING = 10;

    public static final int BOARD_SIZE = 4;
    public static final float DURATION = 0.125f * 1;

    public static int positionToX(int position) {
        return position % BOARD_SIZE;
    }

    public static int positionToY(int position) {
        return position / BOARD_SIZE;
    }
}
