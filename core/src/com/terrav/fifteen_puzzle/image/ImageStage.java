package com.terrav.fifteen_puzzle.image;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import com.terrav.fifteen_puzzle.AppConstants;
import com.terrav.fifteen_puzzle.FifteenPuzzleGame;
import com.terrav.fifteen_puzzle.UiWidgetGroup;
import com.terrav.fifteen_puzzle.classic.BoardPresenter;

public class ImageStage extends Stage {

    public static final float BOARD_SCREEN_SIZE = AppConstants.APP_HEIGHT;
    private Texture texture;

    public ImageStage(Texture texture) {

        super(FifteenPuzzleGame.viewport);
        this.texture = texture;

        Image background = new Image(texture);
        background.setFillParent(true);
        background.setScaling(Scaling.fill);
        addActor(background);

        Container<Actor> rootContainer = new Container<>();
        rootContainer.setFillParent(true);
        rootContainer.center().width(BOARD_SCREEN_SIZE).height(BOARD_SCREEN_SIZE);
        addActor(rootContainer);

        ImageBoardView boardView = new ImageBoardView(texture);
        BoardPresenter boardPresenter = new BoardPresenter();
        boardView.setPresenter(boardPresenter);

        rootContainer.setActor(boardView);

        boardPresenter.start();

        addActor(new UiWidgetGroup());
    }
}
