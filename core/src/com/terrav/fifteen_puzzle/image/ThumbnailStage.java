package com.terrav.fifteen_puzzle.image;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.terrav.fifteen_puzzle.AppAssets;
import com.terrav.fifteen_puzzle.AppConstants;
import com.terrav.fifteen_puzzle.FifteenPuzzleGame;
import com.terrav.fifteen_puzzle.GameScreen;
import com.terrav.fifteen_puzzle.UiWidgetGroup;


public class ThumbnailStage extends Stage {

    private AssetManager assetManager = new AssetManager();
    private boolean taskScheduled = false;
    private ProgressBar progressBar;

    public ThumbnailStage() {

        super(FifteenPuzzleGame.viewport);

        Table thumbnailTable = new Table();
        thumbnailTable.setFillParent(true);
        thumbnailTable.defaults().expand().fill().uniform().space(AppConstants.PADDING);
        addActor(thumbnailTable);

        thumbnailTable.add(getThumbnailImage("background-01"));
        thumbnailTable.add(getThumbnailImage("background-02"));
        thumbnailTable.add(getThumbnailImage("background-03"));
        thumbnailTable.add(getThumbnailImage("background-04"));
        thumbnailTable.row();
        thumbnailTable.add(getThumbnailImage("background-05"));
        thumbnailTable.add(getThumbnailImage("background-06"));
        thumbnailTable.add(getThumbnailImage("background-07"));
        thumbnailTable.add(getThumbnailImage("background-08"));
        thumbnailTable.row();

        thumbnailTable.add();
        thumbnailTable.add(getProgressBar()).height(Value.percentHeight(.5f, thumbnailTable)).colspan(2);
        thumbnailTable.add();

        addActor(new UiWidgetGroup());
    }

    private ProgressBar getProgressBar() {
        ProgressBar.ProgressBarStyle progressBarStyle = new ProgressBar.ProgressBarStyle();
        progressBarStyle.background = AppAssets.skin.getDrawable("border-highlight");
        progressBarStyle.knobBefore = new TiledDrawable(AppAssets.skin.getRegion("progress-bar-fillment")) {
            @Override
            public float getMinWidth() {
                return 0;
            }
        };

        progressBar = new ProgressBar(0, 100, 10, false, progressBarStyle);
        progressBar.setVisible(false);
        return progressBar;
    }

    private Button getThumbnailImage(String imageName) {
        Image image = new Image(AppAssets.skin.getDrawable(imageName + "-thumbnail"));
        image.setScaling(Scaling.fit);

        Button thumbnailButton = new Button(AppAssets.skin.getDrawable("border-highlight"));
        thumbnailButton.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {

                if (taskScheduled) {
                    return;
                }

                String assetName = "background/" + imageName + ".png";
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        progressBar.setValue(progressBar.getValue() + 10);

                        if (assetManager.isLoaded(assetName)) {
                            cancel();

                            FifteenPuzzleGame game = (FifteenPuzzleGame) Gdx.app.getApplicationListener();
                            GameScreen gameScreen = (GameScreen) game.getScreen();
                            gameScreen.setStage(new ImageStage(assetManager.get(assetName)));
                        }
                    }
                }, .025f, .025f);

                progressBar.setVisible(true);
                taskScheduled = true;
                assetManager.load(assetName, Texture.class);
            }
        });

        thumbnailButton.add(image).expand().fill();
        return thumbnailButton;
    }

    @Override
    public void draw() {
        assetManager.update();

        super.draw();
    }
}
