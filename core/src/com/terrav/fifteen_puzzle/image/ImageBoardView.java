package com.terrav.fifteen_puzzle.image;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.terrav.fifteen_puzzle.AppAssets;
import com.terrav.fifteen_puzzle.FifteenPuzzleGame;
import com.terrav.fifteen_puzzle.GameScreen;
import com.terrav.fifteen_puzzle.MainMenuStage;
import com.terrav.fifteen_puzzle.classic.BoardPresenter;
import com.terrav.fifteen_puzzle.image.widget.ImageTileWidget;
import com.terrav.fifteen_puzzle.image.widget.ImageTokenWidget;
import com.terrav.fifteen_puzzle.model.Tile;

import static com.terrav.fifteen_puzzle.AppConstants.BOARD_SIZE;
import static com.terrav.fifteen_puzzle.AppConstants.DURATION;

public class ImageBoardView extends Stack {

    private BoardPresenter boardPresenter;

    private TextureRegion[][] imageBoardTexture;
    private Table backgroundLayer;
    private WidgetGroup foregroundLayer;
    private Texture texture;

    public ImageBoardView(Texture texture) {
        this.texture = texture;
    }

    public void setPresenter(BoardPresenter boardPresenter) {

        this.boardPresenter = boardPresenter;

        imageBoardTexture = initImageBoardTexture();

        Tile[] tiles = boardPresenter.getTiles();
        backgroundLayer = initBoard(tiles);
        add(backgroundLayer);

        foregroundLayer = new WidgetGroup();
        foregroundLayer.setFillParent(true);
        foregroundLayer.setTouchable(Touchable.disabled);

        for (Tile tile : tiles) {
            foregroundLayer.addActor(new ImageTokenWidget(tile.getToken(), imageBoardTexture));
        }
        add(foregroundLayer);

    }

    public void gestureSwipe(Tile tile1, Tile tile2) {
        boardPresenter.gestureSwipe(tile1, tile2);
        animate();
    }

    public void gestureClick(Tile tile) {
        boardPresenter.gestureClick(tile);
        animate();
    }

    private void animate() {
        for (Actor actor : foregroundLayer.getChildren()) {
            ((ImageTokenWidget) actor).animate();
        }
    }

    private Table initBoard(Tile[] tiles) {
        Table boardTable = new Table();
        boardTable.defaults().expand().fill().uniform();

        for (int y = 0; y < BOARD_SIZE; y++) {
            for (int x = 0; x < BOARD_SIZE; x++) {
                int position = y * BOARD_SIZE + x;
                Image tileImage = new ImageTileWidget(getSolidDrawable(Color.BLACK), tiles[position], this);
                tileImage.setPosition(getWidth() / BOARD_SIZE, getHeight() / BOARD_SIZE);

                boardTable.add(tileImage);
            }

            boardTable.row();
        }

        return boardTable;
    }

    private TextureRegion[][] initImageBoardTexture() {
        TextureRegion textureRegion = new TextureRegion(texture);
        int screenWidth = FifteenPuzzleGame.viewport.getScreenWidth();
        int screenHeight = FifteenPuzzleGame.viewport.getScreenHeight();
        float screenProportion = (float) screenWidth / (float) screenHeight;
        float textureProportion = textureRegion.getRegionWidth() / textureRegion.getRegionHeight();

        if (screenProportion > textureProportion) {
            float regionWidth = textureRegion.getRegionWidth();
            float regionHeight = regionWidth * textureProportion / screenProportion;

            textureRegion.setRegionX((int) (textureRegion.getRegionX() + (textureRegion.getRegionWidth() - regionHeight) / 2));
            textureRegion.setRegionY((int) (textureRegion.getRegionY() + (textureRegion.getRegionHeight() - regionHeight) / 2));
            textureRegion.setRegionWidth((int) regionHeight);
            textureRegion.setRegionHeight((int) regionHeight);
        } else {
            float regionHeight = textureRegion.getRegionWidth();
            float regionWidth = regionHeight * screenProportion / textureProportion;

            textureRegion.setRegionX((int) ((textureRegion.getRegionWidth() - regionWidth) / 2));
            textureRegion.setRegionY((int) ((textureRegion.getRegionHeight() - regionWidth) / 2));
            textureRegion.setRegionWidth((int) regionWidth);
            textureRegion.setRegionHeight((int) regionWidth);
        }

        return textureRegion.split(textureRegion.getRegionWidth() / BOARD_SIZE, textureRegion.getRegionHeight() / BOARD_SIZE);
    }

    private Drawable getSolidDrawable(Color color) {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.drawPixel(0, 0, Color.rgba8888(color));

        Texture texture = new Texture(pixmap);
        return new TextureRegionDrawable(new TextureRegion(texture));
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (boardPresenter.isWin() && boardPresenter.isRunning()) {
            boardPresenter.stop();

            int actorNo = 1;
            for (Actor actor : foregroundLayer.getChildren()) {
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        ((ImageTokenWidget) actor).win();
                    }
                }, actorNo++ * DURATION);
            }

            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    Label congrats = new Label(AppAssets.i18n.get("congratulations.key"), AppAssets.skin, "large");
                    congrats.setAlignment(Align.center);
                    congrats.setColor(congrats.getColor().mul(1, 1, 1, 0));
                    congrats.addAction(Actions.fadeIn(DURATION));
                    congrats.addAction(new Action() {
                        private float remainingDuration = DURATION;
                        @Override
                        public boolean act(float delta) {
                            congrats.setFontScale(1 + 1 * (1 - remainingDuration / DURATION));
                            remainingDuration -= delta;

                            return remainingDuration < 0;
                        }
                    });
                    add(congrats);
                }
            }, actorNo++ * DURATION);

            // getStage().addAction(Actions.alpha(.2f, AppConstants.DURATION));

            addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Game game = (Game) Gdx.app.getApplicationListener();
                    GameScreen gameScreen = (GameScreen) game.getScreen();
                    gameScreen.setStage(new MainMenuStage());
                }
            });
        }
    }

    @Override
    public void setSize(float width, float height) {
        float size = Math.min(width, height);
        super.setSize(size, size);
    }
}
