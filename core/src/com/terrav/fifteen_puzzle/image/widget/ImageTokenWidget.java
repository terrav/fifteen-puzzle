package com.terrav.fifteen_puzzle.image.widget;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.terrav.fifteen_puzzle.AppAssets;
import com.terrav.fifteen_puzzle.AppConstants;
import com.terrav.fifteen_puzzle.model.Token;
import com.terrav.fifteen_puzzle.model.VoidToken;

import static com.terrav.fifteen_puzzle.AppConstants.BOARD_SIZE;
import static com.terrav.fifteen_puzzle.AppConstants.DURATION;
import static com.terrav.fifteen_puzzle.AppConstants.positionToX;
import static com.terrav.fifteen_puzzle.AppConstants.positionToY;

public class ImageTokenWidget extends Image {

    public static final int TOKEN_BORDER = 4;
    private Token token;
    private Pixmap pixmap;
    private TextureRegion region;

    public ImageTokenWidget(Token token, TextureRegion[][] imageBoardTexture) {

        this.token = token;

        setTouchable(Touchable.disabled);
        int validPosition = token.getValidPosition();

        TextureRegion region = imageBoardTexture[positionToY(validPosition)][positionToX(validPosition)];
        initDrawable(region);

        if (token instanceof VoidToken) {
            setColor(0, 0, 0, 0);
        }
    }

    public void win() {
        setColor(1, 1, 1, .2f);
        setDrawable(new TextureRegionDrawable(region));
        addAction(Actions.fadeIn(DURATION));
    }

    private void initDrawable(TextureRegion region) {
        this.region = region;

        Texture texture = region.getTexture();
        if (!texture.getTextureData().isPrepared()) {
            texture.getTextureData().prepare();
        }

        pixmap = new Pixmap(region.getRegionWidth() + TOKEN_BORDER * 2, region.getRegionHeight() + TOKEN_BORDER * 2, Pixmap.Format.RGBA8888);
        Pixmap sourcePixmap = texture.getTextureData().consumePixmap();
        pixmap.drawPixmap(sourcePixmap, region.getRegionX(), region.getRegionY(), region.getRegionWidth(), region.getRegionHeight(), TOKEN_BORDER, TOKEN_BORDER, region.getRegionWidth() + TOKEN_BORDER, region.getRegionHeight() + TOKEN_BORDER);

        pixmap.setColor(Color.BLACK);
        for (int i = 0; i < TOKEN_BORDER; i++) {
            if (i >= TOKEN_BORDER / 2) {
                pixmap.setColor(Color.GRAY);
            }
            pixmap.drawRectangle(i, i, pixmap.getWidth() - i, pixmap.getHeight() - i);
        }

        Drawable tokenDrawable = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap)));
        setDrawable(tokenDrawable);
    }

    public void animate() {
        if (!token.isBusy()) {
            return;
        }

        int tokenPosition = token.getPosition();
        float width = getParent().getWidth() / BOARD_SIZE;
        float height = getParent().getHeight() / BOARD_SIZE;

        MoveToAction moveTo = Actions.moveTo(width * positionToX(tokenPosition), height * (BOARD_SIZE - positionToY(tokenPosition) - 1), AppConstants.DURATION);
        RunnableAction run = Actions.run(new Runnable() {
            @Override
            public void run() {
                if (AppAssets.settings.sound) {
                    AppAssets.diceSound.play();
                }
                ImageTokenWidget.this.token.stop();
            }
        });
        addAction(Actions.sequence(moveTo, run));
    }

    @Override
    public void act(float delta) {
/*
        if (token.getTile().isBusy()) {
            setColor(Color.RED);
        } else {
            setColor(Color.WHITE);
        }
*/

        super.act(delta);
    }

    @Override
    public void layout() {
        int tokenPosition = token.getPosition();
        float width = getParent().getWidth() / BOARD_SIZE;
        float height = getParent().getHeight() / BOARD_SIZE;

        setSize(width, height);
        setPosition(width * positionToX(tokenPosition), height * (BOARD_SIZE - positionToY(tokenPosition) - 1));

        super.layout();
    }
}
