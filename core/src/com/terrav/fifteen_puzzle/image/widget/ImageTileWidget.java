package com.terrav.fifteen_puzzle.image.widget;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.terrav.fifteen_puzzle.image.ImageBoardView;
import com.terrav.fifteen_puzzle.model.Tile;


public class ImageTileWidget extends Image {

    private final Tile tile;
    private final ImageBoardView boardView;

    public ImageTileWidget(Drawable drawable, Tile tile, ImageBoardView boardView) {
        super(drawable);

        this.tile = tile;
        this.boardView = boardView;

        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                boardView.gestureClick(tile);
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);

                if (pointer < 0 || fromActor == null || !(fromActor instanceof ImageTileWidget)) {
                    return;
                }

                ImageTileWidget tileWidget = (ImageTileWidget) fromActor;
                if (tile.isBusy() || tileWidget.tile.isBusy()) {
                    return;
                }

                boardView.gestureSwipe(tileWidget.tile, tile);
            }
        });
    }
}
