package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.SerializationException;

public class FifteenPuzzlePreferences {

    private final Json json = new Json();
    private Preferences preferences;

    private Preferences getPreferences() {
        if (preferences == null) {
            preferences = Gdx.app.getPreferences(AppConstants.APP_NAME);
        }
        return preferences;
    }

    public void saveSettings(Settings settings) {
        getPreferences().putString(AppConstants.APP_NAME, json.toJson(settings));
        getPreferences().flush();
    }

    public Settings loadSettings() {
        String storedSettings = getPreferences().getString(AppConstants.APP_NAME);
        Settings settings = null;

        try {
            settings = json.fromJson(Settings.class, storedSettings);
        } catch (SerializationException e) {
            //
        }

        if (settings == null) {
            settings = new Settings();
            settings.sound = true;
            settings.language = Language.ENG;
        }

        return settings;
    }
}
