package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.terrav.fifteen_puzzle.classic.GameStage;
import com.terrav.fifteen_puzzle.image.ThumbnailStage;


public class MainMenuStage extends Stage {

    private final Image title;
    private final TextButton newGameButton;
    private final TextButton imageGameButton;

    public MainMenuStage() {

        super(FifteenPuzzleGame.viewport);

        Table rootTable = new Table();
        rootTable.setFillParent(true);
        rootTable.defaults().uniform().pad(AppConstants.PADDING);
        rootTable.addAction(Actions.fadeIn(AppConstants.DURATION));
        addActor(rootTable);

        title = new Image(AppAssets.skin.getDrawable(AppAssets.i18n.get("title.drawing")));
        rootTable.add(title).uniform(false, false).expand().center();
        rootTable.row();

        newGameButton = new TextButton(AppAssets.i18n.get("classic.key"), AppAssets.skin, "large");
        newGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Game game = (Game) Gdx.app.getApplicationListener();
                GameScreen gameScreen = (GameScreen) game.getScreen();
                gameScreen.setStage(new GameStage());
            }
        });
        rootTable.add(newGameButton).width(Value.percentWidth(.5f, rootTable)).height(Value.percentHeight(.2f, rootTable));
        rootTable.row();

        imageGameButton = new TextButton(AppAssets.i18n.get("photo.key"), AppAssets.skin, "large");
        imageGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Game game = (Game) Gdx.app.getApplicationListener();
                GameScreen gameScreen = (GameScreen) game.getScreen();
                gameScreen.setStage(new ThumbnailStage());
            }
        });
        rootTable.add(imageGameButton).width(Value.percentWidth(.5f, rootTable)).height(Value.percentHeight(.2f, rootTable));
        rootTable.row();

        Button gearButton = new Button(AppAssets.skin.getDrawable("gear"));
        gearButton.setTransform(true);
        gearButton.setOrigin(gearButton.getWidth(), 0);
        gearButton.setPosition(AppConstants.APP_WIDTH - gearButton.getWidth() - AppConstants.PADDING, AppConstants.PADDING);
        gearButton.setScale(.5f, .5f);
        gearButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DialogBox dialogBox = new DialogBox();
                addActor(dialogBox);
            }

        });
        addActor(gearButton);
    }

    public void updateTitle() {
        title.setDrawable(AppAssets.skin.getDrawable(AppAssets.i18n.get("title.drawing")));
        newGameButton.setText(AppAssets.i18n.get("classic.key"));
        imageGameButton.setText(AppAssets.i18n.get("photo.key"));
    }
}
