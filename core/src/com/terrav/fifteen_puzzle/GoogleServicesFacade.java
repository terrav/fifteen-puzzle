package com.terrav.fifteen_puzzle;

public interface GoogleServicesFacade {

    static final int RC_REQUEST = 10001;

    void showAds(boolean show);

    void thankYou(Callback callback);

    void processPurchases(Callback callback);

    void sendEmail();

    interface Callback {

        void call(boolean result);
    }
}
