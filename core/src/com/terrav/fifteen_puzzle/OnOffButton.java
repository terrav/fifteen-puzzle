package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


public class OnOffButton extends TextButton {

    private String onText;
    private String offText;

    public OnOffButton(Skin skin, String styleName) {
        super(AppAssets.i18n.get("off.key"), skin, styleName + "-toggle");

        onText = AppAssets.i18n.get("on.key");
        offText = AppAssets.i18n.get("off.key");

        addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                updateText();
            }
        });
    }

    private void updateText() {
        if (isChecked()) {
            setText(onText);
        } else {
            setText(offText);
        }
    }

    @Override
    public void setChecked(boolean isChecked) {
        super.setChecked(isChecked);
        updateText();
    }
}
