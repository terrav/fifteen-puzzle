package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

public class AppAssets {

    private static final String FONT_CHARS = "абвгдежзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;:,{}\"´`'<>";

    public static Skin skin;
    public static TextureAtlas atlas;

    public static BitmapFont largeFont;
    public static BitmapFont regularFont;
    public static BitmapFont smallFont;

    public static Sound diceSound;
    public static I18NBundle i18n;
    public final static Settings settings = new FifteenPuzzlePreferences().loadSettings();

    public static void load() {
        atlas = new TextureAtlas(Gdx.files.internal("atlas/game.atlas"));
        skin = new Skin(atlas);

        diceSound = Gdx.audio.newSound(Gdx.files.internal("sounds/dice.ogg"));

        i18n();
        generateFonts();
        loadSkin();
    }

    public static void i18n() {
        switch (settings.language) {
            case RUS:
                i18n = I18NBundle.createBundle(Gdx.files.internal("i18n/fifteen-puzzle"), new Locale("ru"));
                break;
            case ENG:
                i18n = I18NBundle.createBundle(Gdx.files.internal("i18n/fifteen-puzzle"), Locale.ENGLISH);
                break;
        }

        Game game = (Game) Gdx.app.getApplicationListener();
        GameScreen gameScreen = (GameScreen) game.getScreen();
        Stage stage = gameScreen == null ? null : gameScreen.getStage();

        if (stage != null && stage instanceof MainMenuStage) {
            ((MainMenuStage) stage).updateTitle();
        }
    }

    public static void saveSettings() {
        new FifteenPuzzlePreferences().saveSettings(settings);
    }

    private static void generateFonts() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Roboto-Bold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.color = Color.WHITE;
        parameter.borderColor = Color.DARK_GRAY;
        parameter.characters = FONT_CHARS;

        parameter.size = 100;
        parameter.borderWidth = 3;
        largeFont = generator.generateFont(parameter);
        parameter.size = 50;
        parameter.borderWidth = 2;
        regularFont = generator.generateFont(parameter);
        parameter.size = 25;
        parameter.borderWidth = 1;
        smallFont = generator.generateFont(parameter);

        skin.add("large-font", largeFont, BitmapFont.class);
        skin.add("regular-font", regularFont, BitmapFont.class);
        skin.add("small-font", smallFont, BitmapFont.class);

        generator.dispose();
    }

    private static void loadSkin() {
        skin.load(Gdx.files.internal("uiskin.json"));
    }

    public static void dispose() {
        skin.dispose();
    }
}
