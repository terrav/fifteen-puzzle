package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class FifteenPuzzleGame extends Game {

    public static final Viewport viewport = new FitViewport(
            AppConstants.APP_WIDTH,
            AppConstants.APP_HEIGHT,
            new OrthographicCamera());

    public final GoogleServicesFacade googleServicesFacade;

    private boolean premium = false;

    public FifteenPuzzleGame(GoogleServicesFacade googleServicesFacade) {
        this.googleServicesFacade = googleServicesFacade;
    }

    public boolean isPremium() {
        return premium;
    }

    public void thankYou(GoogleServicesFacade.Callback callback) {
        googleServicesFacade.thankYou(premium -> {
            this.premium = premium;
            callback.call(premium);
        });
    }

    @Override
    public void create() {
        AppAssets.load();

        GameScreen screen = new GameScreen(this);
        googleServicesFacade.processPurchases(premium -> {
            this.premium = premium;
            setScreen(screen);
        });
    }
}
