package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameScreen extends ScreenAdapter {

    private FifteenPuzzleGame game;
    private Stage currentStage;

    public GameScreen(FifteenPuzzleGame game) {
        this.game = game;
        setStage(new MainMenuStage());
    }

    public void setStage(Stage stage) {
        hide();

        if (currentStage != null) {
            currentStage.dispose();
        }

        currentStage = stage;

        updateAd();

        show();
    }

    public void updateAd() {
        game.googleServicesFacade.showAds(currentStage instanceof MainMenuStage && AppAssets.settings.showAd);
    }

    public Stage getStage() {
        return currentStage;
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(currentStage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        currentStage.act(delta);
        currentStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        FifteenPuzzleGame.viewport.update(width, height);
    }

}
