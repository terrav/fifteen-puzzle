package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


public class UiWidgetGroup extends WidgetGroup {

    public UiWidgetGroup() {
        setFillParent(true);
        setTouchable(Touchable.childrenOnly);

        Button gearButton = new Button(AppAssets.skin.getDrawable("gear"));
        gearButton.setTransform(true);
        gearButton.setOrigin(gearButton.getWidth(), 0);
        gearButton.setPosition(AppConstants.APP_WIDTH - gearButton.getWidth() - AppConstants.PADDING, AppConstants.PADDING);
        gearButton.setScale(.5f, .5f);
        gearButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DialogBox dialogBox = new DialogBox();
                addActor(dialogBox);
            }

        });
        addActor(gearButton);

        Button back = new Button(AppAssets.skin.getDrawable("back"));
        back.setTransform(true);
        back.setPosition(AppConstants.PADDING, AppConstants.PADDING);
        back.setScale(.5f, .5f);
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                FifteenPuzzleGame game = (FifteenPuzzleGame) Gdx.app.getApplicationListener();
                GameScreen gameScreen = (GameScreen) game.getScreen();
                gameScreen.setStage(new MainMenuStage());
            }
        });
        addActor(back);
    }
}
