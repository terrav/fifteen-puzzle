package com.terrav.fifteen_puzzle;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;

public class DialogBox extends Stack {

    private final Settings settings;

    public DialogBox() {

        this.settings = AppAssets.settings;

        setFillParent(true);

        Image backgroundImage = new Image(AppAssets.skin.getDrawable("glass-panel"));
        ClickListener closeListener = new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                remove();
                Game game = (Game) Gdx.app.getApplicationListener();
                GameScreen gameScreen = (GameScreen) game.getScreen();
                gameScreen.updateAd();
            }
        };

        backgroundImage.addListener(closeListener);
        add(backgroundImage);

        Image closeImage = new Image(AppAssets.skin.getDrawable("close"));
        closeImage.setScaling(Scaling.none);
        closeImage.addListener(closeListener);

        Table dialogBoxTable = new Table();
        add(dialogBoxTable);
        Table settingsTable = new Table();
        dialogBoxTable.add(settingsTable).center().width(Value.percentWidth(.5f, this)).height(Value.percentHeight(.8f, this));

        settingsTable.setBackground(AppAssets.skin.getDrawable("green-crystal-border"));

        TextButton menu = new TextButton(AppAssets.i18n.get("settings.key"), AppAssets.skin, "regular");
        Table header = new Table();
        header.setBackground(new TextureRegionDrawable(AppAssets.skin.getRegion("solid-color")).tint(Color.valueOf("e7fecc")));
        header.add().width(Value.percentWidth(.1f, settingsTable));
        header.add(menu).expandX().height(90).top().fill();
        header.add(closeImage).width(Value.percentWidth(.1f, settingsTable));

        settingsTable.add(header).expand().fill();
        settingsTable.row();

        menu.getStyle().up = new TextureRegionDrawable(AppAssets.skin.getRegion("solid-color")).tint(Color.valueOf("e7fecc"));

        Table body = new Table();
        ScrollPane scrollPane = new ScrollPane(body);
        scrollPane.getStyle().background = new TextureRegionDrawable(AppAssets.skin.getRegion("solid-color")).tint(Color.valueOf("455434"));
        settingsTable.add(scrollPane).expand().fill().colspan(3).row();

        body.setTouchable(Touchable.enabled);
        body.defaults().pad(AppConstants.PADDING);
        body.columnDefaults(0).width(Value.percentWidth(.1f, body)).fill();
        body.columnDefaults(1).width(Value.percentWidth(.5f, body)).fill();
        body.columnDefaults(2).width(Value.percentWidth(.3f, body)).fill();

        Image soundImage = new Image(AppAssets.skin.getDrawable("sound"), Scaling.none);
        Label soundLabel = new Label(AppAssets.i18n.get("sound.key"), AppAssets.skin, "small");
        OnOffButton soundButton = new OnOffButton(AppAssets.skin, "menu-button");
        soundButton.setChecked(settings.sound);
        soundButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                settings.sound = soundButton.isChecked();
                AppAssets.saveSettings();
            }
        });

        body.add(soundImage);
        body.add(soundLabel);
        body.add(soundButton);
        body.row();

        Image rateImage = new Image(AppAssets.skin.getDrawable("star"), Scaling.none);
        Label rateLabel = new Label(AppAssets.i18n.get("like.it.key"), AppAssets.skin, "small");
        TextButton rateButton = new TextButton(AppAssets.i18n.get("rate.it.key"), AppAssets.skin, "menu-button");
        rateButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("market://details?id=com.terrav.gem_puzzle");
            }
        });

        body.add(rateImage);
        body.add(rateLabel);
        body.add(rateButton);
        body.row();

        Image suggestionImage = new Image(AppAssets.skin.getDrawable("exclamation"), Scaling.none);
        Label suggestionLabel = new Label(AppAssets.i18n.get("any.suggestions.key"), AppAssets.skin, "small");
        TextButton suggestionButton = new TextButton(AppAssets.i18n.get("write.me.key"), AppAssets.skin, "menu-button");
        suggestionButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                FifteenPuzzleGame game = (FifteenPuzzleGame) Gdx.app.getApplicationListener();
                game.googleServicesFacade.sendEmail();
            }
        });

        body.add(suggestionImage);
        body.add(suggestionLabel);
        body.add(suggestionButton);
        body.row();

        Image languageImage = new Image(AppAssets.skin.getDrawable("language"), Scaling.none);
        Label languageLabel = new Label(AppAssets.i18n.get("language.key"), AppAssets.skin, "small");
        Button enUsLanguageButton = new Button(AppAssets.skin, "toggle-image-button");
        enUsLanguageButton.setProgrammaticChangeEvents(false);
        enUsLanguageButton.add(new Image(AppAssets.skin.getDrawable("usa"))).center();
        enUsLanguageButton.setChecked(settings.language == Language.ENG);
        enUsLanguageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (enUsLanguageButton.isChecked()) {
                    settings.language = Language.ENG;
                    AppAssets.saveSettings();

                    AppAssets.i18n();
                    getParent().addActorBefore(DialogBox.this, new DialogBox());
                    remove();
                }
            }
        });
        Button ruRuLanguageButton = new Button(AppAssets.skin, "toggle-image-button");
        ruRuLanguageButton.setProgrammaticChangeEvents(false);
        ruRuLanguageButton.add(new Image(AppAssets.skin.getDrawable("rus"))).center();
        ruRuLanguageButton.setChecked(settings.language == Language.RUS);
        ruRuLanguageButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (ruRuLanguageButton.isChecked()) {
                    settings.language = Language.RUS;
                    AppAssets.saveSettings();

                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            AppAssets.i18n();
                            getParent().addActorBefore(DialogBox.this, new DialogBox());
                            remove();
                        }
                    }, AppConstants.DURATION);
                }
            }
        });
        ButtonGroup<Button> languageButtonGroup = new ButtonGroup<>(enUsLanguageButton, ruRuLanguageButton);
        Table languageButtonTable = new Table();
        languageButtonTable.pad(AppConstants.PADDING);
        languageButtonTable.defaults().space(AppConstants.PADDING);
        languageButtonTable.add(enUsLanguageButton, ruRuLanguageButton);

        body.add(languageImage);
        body.add(languageLabel);
        body.add(languageButtonTable);
        body.row();

        body.add().minHeight(60).colspan(3).expand().row();

        if (((FifteenPuzzleGame) Gdx.app.getApplicationListener()).isPremium()) {

            Table thanksTable = new Table();
            Image thanksImage1 = new Image(AppAssets.skin.getDrawable("heart"), Scaling.none);
            Image thanksImage2 = new Image(AppAssets.skin.getDrawable("heart"), Scaling.none);
            Label thanksLabel = new Label(AppAssets.i18n.get("thank.you.key"), AppAssets.skin, "small");
            thanksLabel.setAlignment(Align.center);

            thanksTable.add(thanksImage1).fill();
            thanksTable.add(thanksLabel).width(Value.percentWidth(.5f, body)).center().fill();
            thanksTable.add(thanksImage2).fill();
            body.add(thanksTable).colspan(3).center().fill();
            body.row();

        } else {

            Image adImage = new Image(AppAssets.skin.getDrawable("ad"), Scaling.none);
            Label adLabel = new Label(AppAssets.i18n.get("allow.ads.key"), AppAssets.skin, "small");
            OnOffButton adButton = new OnOffButton(AppAssets.skin, "menu-button");
            adButton.setChecked(settings.showAd);
            adButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    settings.showAd = adButton.isChecked();
                    AppAssets.saveSettings();
                }
            });

            body.add(adImage);
            body.add(adLabel);
            body.add(adButton);
            body.row();

            Image thanksImage = new Image(AppAssets.skin.getDrawable("heart"), Scaling.none);
            Label thanksLabel = new Label(AppAssets.i18n.get("thank.you.remove.ad.key"), AppAssets.skin, "small");
            TextButton thanksButton = new TextButton("1$", AppAssets.skin, "menu-button");
            thanksButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    FifteenPuzzleGame game = (FifteenPuzzleGame) Gdx.app.getApplicationListener();
                    game.thankYou(premium -> {
                        getParent().addActorBefore(DialogBox.this, new DialogBox());
                        remove();
                    });
                }
            });

            body.add(thanksImage);
            body.add(thanksLabel);
            body.add(thanksButton);
            body.row();
        }
    }
}
