package com.terrav.fifteen_puzzle.classic;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.terrav.fifteen_puzzle.AppAssets;
import com.terrav.fifteen_puzzle.AppConstants;
import com.terrav.fifteen_puzzle.GameScreen;
import com.terrav.fifteen_puzzle.MainMenuStage;
import com.terrav.fifteen_puzzle.classic.widget.TileWidget;
import com.terrav.fifteen_puzzle.classic.widget.TokenWidget;
import com.terrav.fifteen_puzzle.model.Tile;

import static com.terrav.fifteen_puzzle.AppConstants.BOARD_SIZE;

public class BoardView extends Stack {

    private BoardPresenter boardPresenter;

    private Table backgroundLayer;
    private WidgetGroup foregroundLayer;

    public void setPresenter(BoardPresenter boardPresenter) {

        this.boardPresenter = boardPresenter;

        Tile[] tiles = boardPresenter.getTiles();
        backgroundLayer = initBoard(tiles);
        add(backgroundLayer);

        foregroundLayer = new WidgetGroup();
        foregroundLayer.setFillParent(true);
        foregroundLayer.setTouchable(Touchable.disabled);

        for (Tile tile : tiles) {
            foregroundLayer.addActor(new TokenWidget(tile.getToken()));
        }
        add(foregroundLayer);

    }

    public void gestureSwipe(Tile tile1, Tile tile2) {
        boardPresenter.gestureSwipe(tile1, tile2);
        animate();
    }

    public void gestureClick(Tile tile) {
        boardPresenter.gestureClick(tile);
        animate();
    }

    private void animate() {
        for (Actor actor : foregroundLayer.getChildren()) {
            ((TokenWidget) actor).animate();
        }
    }

    private Table initBoard(Tile[] tiles) {
        Table boardTable = new Table();
        boardTable.defaults().expand().fill().uniform();

        for (int y = 0; y < BOARD_SIZE; y++) {
            for (int x = 0; x < BOARD_SIZE; x++) {
                boolean light = y % 2 + x % 2 == 1;
                String tileName = light ? "light-tile" : "dark-tile";

                int position = y * BOARD_SIZE + x;
                Image tileImage = new TileWidget(AppAssets.skin.getDrawable(tileName), tiles[position], this);
                tileImage.setColor(Color.BLACK);
                tileImage.setPosition(getWidth() / BOARD_SIZE, getHeight() / BOARD_SIZE);

                boardTable.add(tileImage);
            }

            boardTable.row();
        }

        return boardTable;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (boardPresenter.isWin() && boardPresenter.isRunning()) {
            boardPresenter.stop();
            Label congrats = new Label(AppAssets.i18n.get("congratulations.key"), AppAssets.skin, "large");
            congrats.setColor(congrats.getColor().mul(1, 1, 1, 0));
            congrats.addAction(Actions.fadeIn(AppConstants.DURATION));
            add(congrats);

            foregroundLayer.addAction(Actions.alpha(.2f, AppConstants.DURATION));

            addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Game game = (Game) Gdx.app.getApplicationListener();
                    GameScreen gameScreen = (GameScreen) game.getScreen();
                    gameScreen.setStage(new MainMenuStage());
                }
            });
        }
    }

    @Override
    public void setSize(float width, float height) {
        float size = Math.min(width, height);
        super.setSize(size, size);
    }
}
