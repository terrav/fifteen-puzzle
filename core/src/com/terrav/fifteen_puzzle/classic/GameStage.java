package com.terrav.fifteen_puzzle.classic;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.terrav.fifteen_puzzle.AppConstants;
import com.terrav.fifteen_puzzle.FifteenPuzzleGame;
import com.terrav.fifteen_puzzle.UiWidgetGroup;

public class GameStage extends Stage {

    public static final float BOARD_SCREEN_SIZE = AppConstants.APP_HEIGHT;

    public GameStage() {

        super(FifteenPuzzleGame.viewport);

        Container<Actor> rootContainer = new Container<>();
        rootContainer.setFillParent(true);
        rootContainer.center().width(BOARD_SCREEN_SIZE).height(BOARD_SCREEN_SIZE);
        addActor(rootContainer);

        BoardView boardView = new BoardView();
        BoardPresenter boardPresenter = new BoardPresenter();
        boardView.setPresenter(boardPresenter);
        rootContainer.setActor(boardView);

        boardPresenter.start();

        addActor(new UiWidgetGroup());
    }
}
