package com.terrav.fifteen_puzzle.classic;

import com.terrav.fifteen_puzzle.model.Board;
import com.terrav.fifteen_puzzle.model.Tile;

public class BoardPresenter {

    private Board board;
    private boolean run;

    public BoardPresenter() {

        this.board = new Board();
    }

    public void gestureSwipe(Tile tile1, Tile tile2) {
        if (run) {
            board.shift(tile1, tile2);
        }
    }

    public Tile[] getTiles() {
        return board.getTiles();
    }

    public boolean isWin() {
        return board.isWin();
    }

    public void gestureClick(Tile tile) {
        if (run) {
            board.click(tile);
        }
    }

    public void start() {
        run = true;
    }

    public void stop() {
        run = false;
    }

    public boolean isRunning() {
        return run;
    }
}
