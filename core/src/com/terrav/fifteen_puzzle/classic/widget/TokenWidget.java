package com.terrav.fifteen_puzzle.classic.widget;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.terrav.fifteen_puzzle.AppAssets;
import com.terrav.fifteen_puzzle.AppConstants;
import com.terrav.fifteen_puzzle.model.Token;
import com.terrav.fifteen_puzzle.model.VoidToken;

import static com.terrav.fifteen_puzzle.AppConstants.BOARD_SIZE;
import static com.terrav.fifteen_puzzle.AppConstants.positionToX;
import static com.terrav.fifteen_puzzle.AppConstants.positionToY;

public class TokenWidget extends TextButton {

    private Token token;

    public TokenWidget(Token token) {
        super(String.valueOf(token.getValidPosition() + 1), AppAssets.skin);

        this.token = token;

        setTouchable(Touchable.disabled);

        if (token instanceof VoidToken) {
            setColor(0, 0, 0, 0);
        }
    }

    public void animate() {
        if (!token.isBusy()) {
            return;
        }

        int tokenPosition = token.getPosition();
        float width = getParent().getWidth() / BOARD_SIZE;
        float height = getParent().getHeight() / BOARD_SIZE;

        MoveToAction moveTo = Actions.moveTo(width * positionToX(tokenPosition), height * (BOARD_SIZE - positionToY(tokenPosition) - 1), AppConstants.DURATION);
        RunnableAction run = Actions.run(new Runnable() {
            @Override
            public void run() {
                if (AppAssets.settings.sound) {
                    AppAssets.diceSound.play();
                }
                TokenWidget.this.token.stop();
            }
        });
        addAction(Actions.sequence(moveTo, run));
    }

    @Override
    public void act(float delta) {
/*
        if (token.getTile().isBusy()) {
            setColor(Color.RED);
        } else {
            setColor(Color.WHITE);
        }
*/

        super.act(delta);
    }

    @Override
    public void layout() {
        int tokenPosition = token.getPosition();
        float width = getParent().getWidth() / BOARD_SIZE;
        float height = getParent().getHeight() / BOARD_SIZE;

        setSize(width, height);
        setPosition(width * positionToX(tokenPosition), height * (BOARD_SIZE - positionToY(tokenPosition) - 1));

        super.layout();
    }
}
