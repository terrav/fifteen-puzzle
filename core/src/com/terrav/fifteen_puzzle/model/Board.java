package com.terrav.fifteen_puzzle.model;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.terrav.fifteen_puzzle.AppConstants;

import java.util.Iterator;

import static com.terrav.fifteen_puzzle.AppConstants.BOARD_SIZE;
import static com.terrav.fifteen_puzzle.AppConstants.positionToX;
import static com.terrav.fifteen_puzzle.AppConstants.positionToY;

public class Board {

    public static final int TOTAL_POSITIONS = BOARD_SIZE * BOARD_SIZE;

    private BiMap<Tile, Token> tilesTokens;
    private Tile[] tiles;

    public Board() {

        tilesTokens = HashBiMap.create();

        tiles = new Tile[TOTAL_POSITIONS];
        for (int position = 0; position < TOTAL_POSITIONS; position++) {
            tiles[position] = new Tile(position, tilesTokens);
            tiles[position].insertToken(new Token(position, tilesTokens));
        }
        tiles[TOTAL_POSITIONS - 1].removeToken();
        tiles[TOTAL_POSITIONS - 1].insertToken(new VoidToken(TOTAL_POSITIONS - 1, tilesTokens));


        randomize();
    }

    public void randomize() {

        for (int position = 0; position < TOTAL_POSITIONS; position++) {
            Tile tile = tiles[position];
            if (tile.isTokenAtValidPosition()) {
                swapTileWithRandom(tile);
            }
        }

        while (!isSolvable()) {
            swapTileWithRandom(tiles[0]);
        }
    }

    public boolean isShiftable(Tile tile1, Tile tile2) {

        if (!tile1.isNeighbour(tile2)) {
            return false;
        }

        int position1 = tile1.getPosition();
        int position2 = tile2.getPosition();

        if (!isLine(position1, position2)) {
            return false;
        }

        Iterator<Tile> iterator = getTileIterator(position1, position2);
        while (iterator.hasNext()) {
            Tile nextTile = iterator.next();
            if (nextTile.getToken() instanceof VoidToken) {
                return true;
            }
        }

        return false;
    }

    private boolean isLine(int position1, int position2) {
        if (isRow(position1, position2) || isColumn(position1, position2)) {
            return true;
        }

        return false;
    }

    private boolean isColumn(int position1, int position2) {
        if (positionToX(position1) == positionToX(position2)) {
            return true;
        }
        return false;
    }

    private boolean isRow(int position1, int position2) {
        if (positionToY(position1) == positionToY(position2)) {
            return true;
        }

        return false;
    }

    public Tile[] getTiles() {
        return tiles;
    }

    public void shift(Tile tile1, Tile tile2) {
        if (!isShiftable(tile1, tile2)) {
            return;
        }

        Iterator<Tile> tileIterator = getTileIterator(tile1.getPosition(), tile2.getPosition());
        Token token = tile1.removeToken();
        while (tileIterator.hasNext() && !(token instanceof VoidToken)) {
            Tile nextTile = tileIterator.next();
            token = token.moveTo(nextTile);
        }
        token.insertTo(tile1);
    }

    public void click(Tile tile1) {
        Tile voidTokenTile = null;
        for (Tile tile2 : tiles) {
            if (tile2.getToken() instanceof VoidToken) {
                voidTokenTile = tile2;
                break;
            }
        }

        int position1 = tile1.getPosition();
        int position2 = voidTokenTile.getPosition();
        int direction = position1 < position2 ? 1 : -1;
        if (isRow(position1, position2)) {
            shift(tile1, tiles[position1 + direction]);
        }

        if (isColumn(position1, position2)) {
            shift(tile1, tiles[position1 + BOARD_SIZE * direction]);
        }

    }

    public boolean isWin() {
        for (int position = 0; position < TOTAL_POSITIONS; position++) {
            if (!tiles[position].isTokenAtValidPosition()) {
                return false;
            }
        }
        return true;
    }

    private boolean isSolvable() {
        int chaotic = 0;
        for (int position = 0; position < TOTAL_POSITIONS; position++) {
            Token token = tiles[position].getToken();
            int tokenId = token.getValidPosition();

            if (token instanceof VoidToken) {
                chaotic += positionToY(position);
            } else {
                int count = 0;
                for (int pair = position; pair < TOTAL_POSITIONS; pair++) {
                    Token pairToken = tiles[pair].getToken();
                    int pairId = pairToken.getValidPosition();
                    if (pairId < tokenId && !(pairToken instanceof VoidToken)) {
                        count++;
                    }
                }

                chaotic += count;
            }
        }

        return chaotic % 2 == 1;
    }

    private Iterator<Tile> getTileIterator(final int position1, final int position2) {
        return new Iterator<Tile>() {
            private int lastPosition = position1;
            private int nextPosition = position2;
            private int step = position2 - position1;

            @Override
            public boolean hasNext() {
                if (nextPosition < 0 || nextPosition >= TOTAL_POSITIONS) {
                    return false;
                }

                if (step == 1 || step == -1) {
                    return positionToY(nextPosition) == positionToY(lastPosition);
                } else if (step == BOARD_SIZE || step == -BOARD_SIZE) {
                    return positionToX(nextPosition) == positionToX(lastPosition);
                }

                throw new IllegalStateException();
            }

            @Override
            public Tile next() {
                if (!hasNext()) {
                    throw new IllegalStateException();
                }

                lastPosition = nextPosition;
                nextPosition = nextPosition + step;

                return tiles[lastPosition];
            }
        };
    }

    private void swapTileWithRandom(Tile tile1) {
        int currentPosition = tile1.getPosition();
        int randomPosition = AppConstants.RANDOM_XS128.nextInt(BOARD_SIZE * BOARD_SIZE);
        while (randomPosition == currentPosition) {
            randomPosition = AppConstants.RANDOM_XS128.nextInt(BOARD_SIZE * BOARD_SIZE);
        }

        Tile tile2 = tiles[randomPosition];
        swap(tile1, tile2);
    }

    private void swap(Tile tile1, Tile tile2) {
        Token token1 = tile1.removeToken();
        Token token2 = tile2.removeToken();

        tile1.insertToken(token2);
        tile2.insertToken(token1);
    }
}
