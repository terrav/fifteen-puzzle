package com.terrav.fifteen_puzzle.model;

import com.google.common.collect.BiMap;

public class Token {

    private int validPosition;
    private BiMap<Token, Tile> tokensTiles;
    private boolean busy;

    public Token(int position, BiMap<Tile, Token> tilesTokens) {

        this.validPosition = position;
        this.tokensTiles = tilesTokens.inverse();
    }

    public boolean isAtValidPosition() {
        return validPosition == getPosition();
    }

    public int getPosition() {
        return tokensTiles.get(this).getPosition();
    }

    public int getValidPosition() {
        return validPosition;
    }

    public boolean isOnBoard() {
        return tokensTiles.get(this) != null;
    }

    public Tile getTile() {
        return tokensTiles.get(this);
    }

    public boolean isBusy() {
        return busy;
    }

    public Token moveTo(Tile tile) {
        start();
        Token tileToken = tile.removeToken();
        tile.insertToken(this);
        return tileToken;
    }

    public void insertTo(Tile tile) {
        start();
        tile.insertToken(this);
    }

    public void stop() {
        busy = false;
    }

    private void start() {
        busy = true;
    }
}
