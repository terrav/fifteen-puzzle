package com.terrav.fifteen_puzzle.model;

import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;

import java.util.Arrays;
import java.util.List;

import static com.terrav.fifteen_puzzle.AppConstants.BOARD_SIZE;

public class Tile {

    private final int position;
    private final BiMap<Tile, Token> tilesTokens;

    public Tile(int position, BiMap<Tile, Token> tilesTokens) {
        this.position = position;
        this.tilesTokens = tilesTokens;
    }

    public boolean isTokenAtValidPosition() {
        return getToken().getValidPosition() == position;
    }

    public boolean isNeighbour(Tile tile) {
        List<Integer> neighbourPositions = Arrays.asList(position - 1, position + 1, position - BOARD_SIZE, position + BOARD_SIZE);
        return neighbourPositions.contains(tile.position);
    }

    public boolean isBusy() {
        return getToken().isBusy();
    }

    public int getPosition() {
        return position;
    }

    public void insertToken(Token token) {
        Preconditions.checkState(!token.isOnBoard());
        Preconditions.checkState(isEmpty());
        tilesTokens.put(this, token);
    }

    public Token removeToken() {
        Preconditions.checkState(!isEmpty());
        Token token = tilesTokens.get(this);
        tilesTokens.remove(this);

        return token;
    }

    public Token getToken() {
        return tilesTokens.get(this);
    }

    private boolean isEmpty() {
        return tilesTokens.get(this) == null;
    }
}