package com.terrav.fifteen_puzzle.model;

import com.google.common.collect.BiMap;

public class VoidToken extends Token {

    public VoidToken(int position, BiMap<Tile, Token> tilesTokens) {
        super(position, tilesTokens);
    }
}
