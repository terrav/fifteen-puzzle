package com.terrav.fifteen_puzzle;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.vending.util.IabHelper;
import com.android.vending.util.IabResult;
import com.android.vending.util.Inventory;
import com.android.vending.util.Purchase;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AndroidLauncher extends AndroidApplication implements GoogleServicesFacade {

    private final int HIDE_ADS = 0;
    private final int SHOW_ADS = 1;
    protected AdView adView;
    protected Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_ADS: {
                    adView.setVisibility(View.VISIBLE);
                    break;
                }
                case HIDE_ADS: {
                    adView.setVisibility(View.GONE);
                    break;
                }
            }
        }
    };

    private IabHelper mHelper;
    private boolean mAdsRemoved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBilling();

        // Create the layout
        RelativeLayout layout = new RelativeLayout(this);

        // Do the stuff that initialize() would do for you
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        // Create the libgdx View
        View gameView = initializeForView(new FifteenPuzzleGame(this));

        // Create and setup the AdMob view
        adView = new AdView(this); // Put in your secret key here
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId("ca-app-pub-1920910478522585/2643442154");

        AdRequest.Builder builder = new AdRequest.Builder();
        builder.addTestDevice("C232D88EAAD312E142868CB9A6745B20");
        builder.addTestDevice("F80F2A27F0E8D31720D7F80AAEFA07AD");
        adView.loadAd(builder.build());

        // Add the libgdx view
        layout.addView(gameView);

        // Add the AdMob view
        RelativeLayout.LayoutParams adParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        layout.addView(adView, adParams);

        // Hook it all up
        setContentView(layout);
    }

    private void initBilling() {
        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(this, AppConstants.PUBLIC_RSA_KEY);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d("IAB", "Problem setting up In-app Billing: " + result);
                }
                // Hooray, IAB is fully set up!
                Log.d("IAB", "Billing Success: " + result);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    @Override
    public void showAds(boolean show) {
        handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
    }

    @Override
    public void thankYou(final Callback callback) {
        if (!mHelper.isSetupDone()) {
            callback.call(false);
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
                    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                        if ( purchase == null) return;
                        Log.d("IAB", "Purchase finished: " + result + ", purchase: " + purchase);

                        // if we were disposed of in the meantime, quit.
                        if (mHelper == null) return;

                        if (result.isFailure()) {
                            //complain("Error purchasing: " + result);
                            //setWaitScreen(false);
                            return;
                        }
//            if (!verifyDeveloperPayload(purchase)) {
//                //complain("Error purchasing. Authenticity verification failed.");
//                //setWaitScreen(false);
//                return;
//            }

                        Log.d("IAB", "Purchase successful.");

                        if (purchase.getSku().equals(AppConstants.SKU_THANK_YOU)) {
                            // bought the premium upgrade!
                            Log.d("IAB", "Purchase is premium upgrade. Congratulating user.");

                            // Do what you want here maybe call your game to do some update
                            //
                            // Maybe set a flag to indicate that ads shouldn't show anymore
                            mAdsRemoved = true;
                            callback.call(mAdsRemoved);
                        }
                    }
                };

                mHelper.launchPurchaseFlow(AndroidLauncher.this, AppConstants.SKU_THANK_YOU, RC_REQUEST, mPurchaseFinishedListener, "HANDLE_PAYLOADS");
            }
        });
    }

    @Override
    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);

        if (mHelper != null) {
            // Pass on the activity result to the helper for handling
            if (mHelper.handleActivityResult(request, response, data)) {
                Log.d("IAB", "onActivityResult handled by IABUtil.");
            }
        }
    }

    @Override
    public void processPurchases(final Callback callback) {
        if (!mHelper.isSetupDone()) {
            callback.call(false);
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Listener that's called when we finish querying the items and subscriptions we own
                IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                        Log.d("IAB", "Query inventory finished.");

                        // Have we been disposed of in the meantime? If so, quit.
                        if (mHelper == null) return;

                        // Is it a failure?
                        if (result.isFailure()) {
                            // handle failure here
                            return;
                        }

                        // Do we have the premium upgrade?
                        Purchase removeAdPurchase = inventory.getPurchase(AppConstants.SKU_THANK_YOU);
                        mAdsRemoved = removeAdPurchase != null;

                        callback.call(mAdsRemoved);
                    }
                };

                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    @Override
    public void sendEmail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Intent.ACTION_SENDTO);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"cziberpv@gmail.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
                i.putExtra(Intent.EXTRA_TEXT   , "Hi mate,");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(AndroidLauncher.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
