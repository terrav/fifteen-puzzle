package com.terrav.fifteen_puzzle.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class AtlasGenerator {

    public static void main(String[] args) {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.maxWidth = 2048;
        settings.maxHeight = 1024;
        TexturePacker.process(settings, "images", "atlas", "game");
    }
}
