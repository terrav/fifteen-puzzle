package com.terrav.fifteen_puzzle.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.terrav.fifteen_puzzle.FifteenPuzzleGame;
import com.terrav.fifteen_puzzle.GoogleServicesFacade;

public class DesktopLauncher {

    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1600;
        config.height = 900;
        config.resizable = false;
        new LwjglApplication(new FifteenPuzzleGame(getCore2AndroidStub()), config);
    }

    private static GoogleServicesFacade getCore2AndroidStub() {
        return new GoogleServicesFacade() {
            @Override
            public void showAds(boolean show) {
                //
            }

            @Override
            public void thankYou(Callback callback) {
                callback.call(true);
            }

            @Override
            public void processPurchases(Callback callback) {
                callback.call(false);
            }

            @Override
            public void sendEmail() {
                //
            }
        };
    }
}
